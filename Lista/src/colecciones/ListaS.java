package colecciones;


/**
 * Write a description of class ListaS here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ListaS<T>
{
    // instance variables - replace the example below with your own
    
    private Nodo<T> cabeza=null;
    private int tamanio=0;
    
    
    

    /**
     * Constructor for objects of class ListaS
     */
    public ListaS()
    {
    
    }
    
    
    public void insertarInicio(T info)
    {
    
        this.cabeza=new Nodo<T>(info, this.cabeza);
        this.tamanio++;
        
    
    }
    
    
    public void insertarFinal(T info)
    {
    
            if(this.esVacio())
                this.insertarInicio(info);
            else
            {
            try{
            this.getPos(this.tamanio-1).setSig(new Nodo<T>(info,null));
            this.tamanio++;
             }catch(Exception e)
             {
                
                 System.err.println(e.getMessage());
             }
            
            }
    
    }
    
    
    
    
   private Nodo<T> getPos(int i) throws Exception
   {
    
       if(this.esVacio() || i>=this.tamanio || i<0)
            throw new Exception("Indice fuera de rango para la lista simple");
         
        
          Nodo<T> pos=this.cabeza;
          
          while(i>0)
          {
              pos=pos.getSig();
              i--;
          }
   
    return pos;
   }
    
    
    public boolean esVacio()
    {
        return this.cabeza==null;
    }
    
    
    
    public String toString()
    {
        if(this.esVacio())
            return "Lista vacía";
        
        String msg="";
        //for i=0; i<length;i++
        for(Nodo<T> x=this.cabeza; x!=null; x=x.getSig())
        {
            msg+=x.getInfo().toString()+"->";
        }
    return msg+"null";
    }
    
    
    
    public T get(int i)
    {
    
        
         try{
                return this.getPos(i).getInfo();

             }catch(Exception e)
             {
                
                 System.err.println(e.getMessage());
                 return null;
             }
        
    }

    
    
    public void set(int i, T info)
    {
    
        
         try{
                this.getPos(i).setInfo(info);

             }catch(Exception e)
             {
                
                 System.err.println(e.getMessage());

             }
        
    }
    
    public int getTamanio()
    {
        return this.tamanio;
    }
    
    
    
    public void concatAntes(ListaS<T> l2)
    {
    if(l2.esVacio()) throws new Exception("Lista/2/esta vacia")
      else{  Nodo<T> auxFinal = new Nodo();
        auxFinal = l2.cabeza;
        while(auxFinal.getSig() != null)
        {
            auxFinal=auxFinal.getSig();
        }
        auxFinal.setSig(this.cabeza);
        this.cabeza = l2.cabeza;
        tamanio+=l2.getTamanio();
    }}
}
